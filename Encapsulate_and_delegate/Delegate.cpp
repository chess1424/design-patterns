#include <iostream>

using namespace std;

class QuackBehavior{
public:
  virtual void quack() = 0;
};

class FlyBehavior{
public:
  virtual void fly() = 0;
};

class Quack : public QuackBehavior{
public:
  void quack(){
    cout << "Quacking" << endl;
  }
};

class NoQuack : public QuackBehavior{
public:
  void quack(){
    cout << "I can't quack" << endl;
  }
};

class FlyWithWings : public FlyBehavior{
public:
  void fly(){
    cout << "Flying with wings" << endl;
  }
};

class Duck{
public:
  void swim(){
    cout << "Swimming" << endl;
  }
};

class MallarDuck{
public:
  FlyBehavior *flyBehavior;
  QuackBehavior *quackBehavior;

  MallarDuck(QuackBehavior *quackBehavior, FlyBehavior *flyBehavior){
   this -> flyBehavior = flyBehavior;
   this -> quackBehavior = quackBehavior;
  }

  void display(){
    cout << "I'm a Mallar duck" << endl;
  }

};

int main()
{
  QuackBehavior *quackBehavior = new NoQuack();
  FlyBehavior *flyBehavior = new FlyWithWings();
  MallarDuck mallarDuck(quackBehavior, flyBehavior);
  mallarDuck.display();
  mallarDuck.quackBehavior->quack();
  mallarDuck.flyBehavior->fly();

  return 0;
}