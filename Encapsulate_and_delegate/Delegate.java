class Duck{//Super class
  QuackBehavior quackBehavior;
  FlyBehavior flyBehavior;

  public void swim(){
    System.out.println("Swimming");
  }

  public void display(){
    System.out.println("I'm  a Duck class");
  }
}

class MallarDuck extends Duck{//Child class that uses interfaces.
  public MallarDuck(QuackBehavior quackBehavior, FlyBehavior flyBehavior){
    this.quackBehavior = quackBehavior;
    this.flyBehavior = flyBehavior;
  }

  public void display(){
    System.out.println("I'm a MallarDuck");
  }

}

interface QuackBehavior{//Interface to encapsulate Quack behavior
  public void quack();
}

interface FlyBehavior{//Interface to encapsulate Fly behavior
  public void fly();
}

class Quack implements QuackBehavior{
  public void quack(){
    System.out.println("Quacking");
  }
}

class NoQuack implements QuackBehavior{
  public void quack(){
    System.out.println("I can't quack");
  }
}

class FlyWithWings implements FlyBehavior{
  public void fly(){
    System.out.println("Flying with wings");
  }
}

class NoFly implements FlyBehavior{
  public void fly(){
    System.out.println("I can't fly");
  }
}

public class Delegate{
  public static void main(String[] args){
    QuackBehavior quackBehavior = new Quack();
    FlyBehavior flyBehavior = new NoFly();
    MallarDuck mallarDuck = new MallarDuck(quackBehavior, flyBehavior);
    mallarDuck.display();
    mallarDuck.quackBehavior.quack();
    mallarDuck.flyBehavior.fly();
  }
}