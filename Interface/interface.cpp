#include <iostream>

using namespace std;

class Quackable{
public:
  virtual void quack() = 0;
};

class Duck{
public:
  void display(){
    cout << "Displaying" << endl;
  }
};

class MallarDuck : public Duck, public Quackable{
public:
  void quack(){
    cout  << "Quack" << endl;
  }
};

int main(){
  MallarDuck mallarDuck;
  mallarDuck.quack();
  mallarDuck.display();
  return 0;
}