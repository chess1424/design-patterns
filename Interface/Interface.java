
interface Quackable{
  public void quack();
}

interface Flyable{
  public void fly();
}

class Duck{
  public void display(){
    System.out.println("Displaying");
  }
}

class MallarDuck extends Duck implements Quackable, Flyable{
  public void quack(){
    System.out.println("Quack");
  }

  public void fly(){
    System.out.println("Flying");
  }
}

public class Interface{
  public static void main(String[] args){
    MallarDuck mallarDuck = new MallarDuck();
    mallarDuck.display();
    mallarDuck.quack();
    mallarDuck.fly();
  }
}