class Duck
  def swim
    puts "Swimming"
  end
end

class MallarDuck < Duck
  def fly
    puts "Flying"
  end
end

duck = Duck.new
duck.swim

mallarDuck = MallarDuck.new
mallarDuck.swim
mallarDuck.fly
