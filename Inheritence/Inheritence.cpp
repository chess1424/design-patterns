#include <iostream>

using namespace std;

class Duck{
public:
  void swim(){
    cout << "Swimming" << endl;
  }
};

class MallarDucK : public Duck{
public:
  void fly(){
    cout << "Flying" << endl;
  }
};

int main(){

  Duck duck;
  duck.swim();
  MallarDucK mallarDuck;
  mallarDuck.swim();
  mallarDuck.fly();
  return 0;
}