
class Duck{
    public void swim(){
        System.out.println("Swimming");
    }
}

class MallarDuck extends Duck{
    public void fly(){
        System.out.println("Flying");
    }
}

public class Inheritence{
  public static void main(String[] args) {

    Duck duck = new Duck();
    duck.swim();

    MallarDuck mallarDuck = new MallarDuck();
    mallarDuck.swim();
    mallarDuck.fly();
  }
}